defmodule FluxAMQP do
  @moduledoc """
  An interface to connect to AMQP broker, sending and retrieving messages.
  """
  @moduledoc since: "0.0.1"

  alias FluxAMQP.{Connector, Sender}

  @doc """
  Closes an open Connection.

  ## Parameters

    - `connection` - The `t:AMQP.Connection.t/0` struct.

  ## Examples

      iex> connection = FluxAMQP.connect!(__MODULE__, only_connect?: true)
      ...> FluxAMQP.close_connection(connection)
      :ok
  """
  @doc since: "0.0.1"
  @spec close_connection(AMQP.Connection.t()) :: :ok | {:error, :failed_to_close_connection}
  def close_connection(connection) do
    Connector.close_connection(connection)
  end

  @doc """
  Closes an open Connection.

  Similar to `close_connection/1`, but raises `t:FluxAMQP.Error.t/0` on
  failure.

  ## Examples

      iex> connection = FluxAMQP.connect!(__MODULE__, only_connect?: true)
      ...> FluxAMQP.close_connection!(connection)
      :ok
  """
  @doc since: "0.0.5"
  @spec close_connection!(AMQP.Connection.t()) :: :ok
  def close_connection!(connection) do
    Connector.close_connection!(connection)
  end

  @doc """
  Open a `t:AMQP.Connection.t/0` and configure it.

  ## Parameters

    - `consumer` - The module which will consume AMQP messages. Accepts
      `t:module/0`.

    - `opts` - Keyword list with connection settings.
      Definition is the same as
      [application configuration](readme.html#application-configuration)
      and the opts defined here are merged with the application configuration.
      Additionally, if the option `:only_connect?` is `true`, this function
      will not configure channel and will return a `t:AMQP.Connection.t/0`,
      otherwise will configure a `t:AMQP.Channel.t/0` and will return it. Can
      be blank. Accepts `t:keyword/0`.

  ## Examples

      iex> result = FluxAMQP.connect(__MODULE__, only_connect?: true)
      ...> with {:ok, %AMQP.Connection{}} <- result, do: :passed
      :passed

      iex> result = FluxAMQP.connect(__MODULE__)
      ...> with {:ok, %AMQP.Channel{}} <- result, do: :passed
      :passed
  """
  @doc since: "0.0.1"
  @spec connect(module, keyword) ::
          {:ok, AMQP.Connection.t() | AMQP.Channel.t()} | {:error, :failed_to_connect}
  def connect(consumer, opts \\ []) do
    Connector.connect(consumer, opts)
  end

  @doc """
  Open a `t:AMQP.Connection.t/0` and configure it.

  Similar to `connect/2`, but raises `t:FluxAMQP.Error.t/0` on failure.

  ## Examples

      iex> result = FluxAMQP.connect!(__MODULE__, only_connect?: true)
      ...> with %AMQP.Connection{} <- result, do: :passed
      :passed

      iex> result = FluxAMQP.connect!(__MODULE__)
      ...> with %AMQP.Channel{} <- result, do: :passed
      :passed
  """
  @doc since: "0.0.5"
  @spec connect!(module, keyword) :: AMQP.Connection.t() | AMQP.Channel.t()
  def connect!(consumer, opts \\ []) do
    Connector.connect!(consumer, opts)
  end

  @doc """
  Send AMQP message.

  ## Parameters

    - `payload` - The message. Accepts `t:String.t/0`.

    - `routing_key` - The AMQP routing key which the message will be sent.
      Accepts `t:String.t/0`.

    - `opts` - Keyword list with options. Can be blank. Accepts `t:keyword/0`.

  ## Examples

      iex> FluxAMQP.send("Hello, World!", "some.amqp.route")
      :ok

  ## Options

    - `:broker` - Keyword list with broker connection settings. Definition is
      the same as
      [application configuration](readme.html#application-configuration)
      and the opts defined here are merged with the broker application
      configuration. Accepts `t:keyword/0`.

    - `:channel` - A valid `t:AMQP.Channel.t/0` struct. If not set, a new
      channel will be created.

    - `:close_connection?` - If `true`, it will close the connection. Defaults
      to `true`. Accepts `t:boolean/0`.

    - `:connection` - A valid `t:AMQP.Connection.t/0` struct. If not set, a
      new connection will be created.

    - `:publish_options` - A Keyword list with valid `AMQP.Basic.publish/5`
      options. Defaults to an `[persistent: true]`. Accepts `t:keyword/0`.
  """
  @doc since: "0.0.1"
  def send(payload, routing_key, opts \\ []) do
    Sender.send(payload, routing_key, opts)
  end

  @doc """
  Send AMQP message.

  Similar to `send/3`, but raises `t:FluxAMQP.Error.t/0` on failure.

  ## Examples

      iex> FluxAMQP.send!("Hello, World!", "some.amqp.route")
      :ok
  """
  @doc since: "0.0.5"
  def send!(payload, routing_key, opts \\ []) do
    Sender.send!(payload, routing_key, opts)
  end

  @doc """
  Open a `t:AMQP.Channel.t/0` based on `t:AMQP.Connection.t/0`.

  ## Parameters

    - `connection` - The `t:AMQP.Connection.t/0` struct.

  ## Examples

    iex> connection = FluxAMQP.connect!(__MODULE__, only_connect?: true)
    ...> result = FluxAMQP.open_channel(connection)
    ...> with {:ok, %AMQP.Channel{}} <- result, do: :passed
    :passed
  """
  @doc since: "0.0.5"
  @spec open_channel(AMQP.Connection.t()) ::
          {:ok, AMQP.Channel.t()} | {:error, :failed_to_open_channel}
  def open_channel(connection) do
    Connector.open_channel(connection)
  end

  @doc """
  Open a `t:AMQP.Channel.t/0` based on `t:AMQP.Connection.t/0`.

  Similar to `open_channel/1`, but raises `t:FluxAMQP.Error.t/0` on failure.

  ## Examples

    iex> connection = FluxAMQP.connect!(__MODULE__, only_connect?: true)
    ...> result = FluxAMQP.open_channel!(connection)
    ...> with %AMQP.Channel{} <- result, do: :passed
    :passed
  """
  @doc since: "0.0.5"
  @spec open_channel!(AMQP.Connection.t()) :: AMQP.Channel.t()
  def open_channel!(connection) do
    Connector.open_channel!(connection)
  end
end
