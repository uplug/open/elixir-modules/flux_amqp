defmodule FluxAMQP.Config do
  @moduledoc false
  @moduledoc since: "0.0.1"

  alias FluxAMQP.Config
  alias FluxAMQP.Config.Broker

  @type t :: %Config{
          broker: Broker.t(),
          routing_keys: list(String.t() | {String.t(), String.t()})
        }

  defstruct broker: %Broker{}, routing_keys: []

  @doc since: "0.0.1"
  @spec fetch(Config.t(), keyword) :: Config.t()
  def fetch(config \\ %Config{}, opts \\ []) do
    case opts do
      [] -> config
      opts -> struct(config, broker: broker(config, opts), routing_keys: routing_keys(opts))
    end
  end

  defp broker(config, opts) do
    Broker.fetch(config.broker, Keyword.get(opts, :broker, []))
  end

  defp routing_keys(opts) do
    Keyword.get(opts, :routing_keys, [])
  end
end
