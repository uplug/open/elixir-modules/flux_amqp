defmodule FluxAMQP.Error do
  @moduledoc """
  `FluxAMQP` error module.
  """
  @moduledoc since: "0.0.5"

  alias FluxAMQP.Error
  alias FluxAMQP.Error.Messages

  @typedoc "`FluxAMQP` error struct"
  @typedoc since: "0.0.5"
  @type t :: %Error{
          reason: atom,
          metadata: map
        }

  defexception [:reason, :metadata]

  @doc false
  @doc since: "0.0.5"
  @spec from(Exception.t() | atom, atom, keyword) :: Error.t()
  def from(error, reason, metadata \\ []) do
    %Error{reason: reason, metadata: Map.new([detail: error] ++ metadata)}
  end

  @doc """
  Generate message `t:String.t/0` based on `:reason`.

  ## Parameters

    - `error` - The `t:FluxAMQP.Error.t/0`.

  ## Examples

      iex> error = %FluxAMQP.Error{reason: :failed_to_connect}
      ...> FluxAMQP.Error.message(error)
      "failed to connect to AMQP broker"
  """
  @doc since: "0.0.5"
  @spec message(Error.t()) :: String.t()
  def message(error) do
    Messages.get(error.reason)
  end

  @doc """
  Generate  `{:error, atom}` based on `:reason`.

  ## Parameters

    - `error` - The `t:FluxRedis.Error.t/0`.

  ## Examples

      iex> error = %FluxAMQP.Error{reason: :failed_to_connect}
      ...> FluxAMQP.Error.to_tuple(error)
      {:error, :failed_to_connect}
  """
  @doc since: "0.0.5"
  @spec to_tuple(Error.t()) :: {:error, atom}
  def to_tuple(error) do
    {:error, error.reason}
  end
end
