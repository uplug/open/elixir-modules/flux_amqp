defmodule FluxAMQP.Sender do
  @moduledoc false
  @moduledoc since: "0.0.1"

  alias AMQP.{Basic, Channel, Connection, Queue}

  alias FluxAMQP.{Connector, Error}

  @doc since: "0.0.1"
  @spec send(String.t(), String.t(), keyword) :: :ok | {:error, :failed_to_send_amqp_message}
  def send(payload, routing_key, opts \\ []) do
    send!(payload, routing_key, opts)
  rescue
    error -> Error.to_tuple(error)
  end

  @doc since: "0.0.5"
  @spec send!(String.t(), String.t(), keyword) :: :ok
  def send!(payload, routing_key, opts \\ []) do
    connection = maybe_open_connection!(opts)

    connection
    |> maybe_open_channel!(opts)
    |> declare_queue!(routing_key)
    |> do_send!(payload, routing_key, opts)

    maybe_close_connection!(connection, opts)
  rescue
    error -> reraise Error.from(error, :failed_to_send_amqp_message), __STACKTRACE__
  end

  defp maybe_open_connection!(opts) do
    case Keyword.get(opts, :connection) do
      %Connection{} = connection ->
        connection

      _ ->
        opts = Keyword.merge(opts, only_connect?: true)
        {:ok, connection} = Connector.connect(__MODULE__, opts)
        connection
    end
  end

  defp maybe_open_channel!(connection, opts) do
    case Keyword.get(opts, :channel) do
      %Channel{} = channel ->
        channel

      _ ->
        {:ok, channel} = Connector.open_channel(connection)
        channel
    end
  end

  defp declare_queue!(channel, route) do
    Queue.declare(channel, route, durable: true)
    channel
  end

  defp do_send!(channel, payload, route, opts) do
    options = Keyword.get(opts, :publish_options, persistent: true)
    :ok = Basic.publish(channel, "", route, payload, options)
    :ok
  end

  defp maybe_close_connection!(connection, opts) do
    if Keyword.get(opts, :close_connection?, true) == true do
      FluxAMQP.close_connection(connection)
    end

    :ok
  end
end
