defmodule FluxAMQP.ConsumerTestUtils.MyConsumer do
  @moduledoc false
  @moduledoc since: "0.0.4"

  use FluxAMQP.Consumer

  @impl FluxAMQP.Consumer
  @spec routing_keys :: list(String.t())
  def routing_keys do
    ["test.route"]
  end

  @impl FluxAMQP.Consumer
  @spec consume(AMQP.Channel.t(), String.t(), map) :: :ok
  def consume(_channel, _payload, _meta) do
    Logger.info("MyConsumer received a message")
    :ok
  end
end
