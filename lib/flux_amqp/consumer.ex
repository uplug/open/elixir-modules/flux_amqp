defmodule FluxAMQP.Consumer do
  @moduledoc """
  Module responsible to consume AMQP messages.

  The main approach to consume is done by defining a module which extends
  `FluxAMQP.Consumer` and implements `c:routing_keys/0` and `c:consume/3`
  functions:

  ```elixir
  defmodule MyApp.AMQP.Consumer do
    use FluxAMQP.Consumer

    @impl FluxAMQP.Consumer
    def routing_keys do
      [
        "route.example",
        {"another.route.example", "amq.direct"}
      ]
    end

    @impl FluxAMQP.Consumer
    def consume(channel, delivery_tag, routing_key, payload) do
      # Handle message
    end
  end
  ```

  Then, set the connection as a worker child on your application:

  ```elixir
  defmodule MyApp.Application do
    use Application

    @impl Application
    def start(_type, _args) do
      import Supervisor.Spec

      children = [
        worker(MyApp.AMQP.Consumer, [])
      ]

      opts = [strategy: :one_for_one]

      Supervisor.start_link(children, opts)
    end
  end
  ```

  If `c:routing_keys/0` returns an empty list, `FluxAMQP` will try to extract
  the routing keys from the
  [application configuration](readme.html#application-configuration).
  """
  @moduledoc since: "0.0.1"

  alias AMQP.Channel

  defmacro __using__(_opts) do
    quote do
      use GenServer
      use AMQP

      alias AMQP.Channel

      require Logger

      @behaviour FluxAMQP.Consumer

      @spec start_link :: {:ok, pid} | :ignore | {:error, any}
      def start_link, do: GenServer.start_link(__MODULE__, [], [])

      @spec init(any) :: {:ok, AMQP.Channel.t()}

      @impl GenServer
      def init(_opts) do
        channel = connect!()
        Process.monitor(channel.conn.pid)
        {:ok, channel}
      end

      @impl GenServer
      def handle_info({:DOWN, _data, :process, _pid, _reason}, _state) do
        Logger.error("AMQP consumer '#{__MODULE__}' lost connection with AMQP broker")
        {:noreply, connect!()}
      end

      @impl GenServer
      def handle_info({:basic_consume_ok, _consumer_tag}, channel) do
        Logger.info("AMQP consumer '#{__MODULE__}' ready to consume messages")
        {:noreply, channel}
      end

      @impl GenServer
      def handle_info({:basic_cancel, _consumer_tag}, channel) do
        Logger.info("AMQP consumer '#{__MODULE__}' cancelled by AMQP broker")
        {:stop, :normal, channel}
      end

      @impl GenServer
      def handle_info({:basic_cancel_ok, _consumer_tag}, channel) do
        Logger.info("AMQP consumer '#{__MODULE__}' cancelled locally")
        {:noreply, channel}
      end

      @impl GenServer
      def handle_info({:basic_deliver, payload, meta}, channel) do
        spawn(fn -> consume(channel, payload, meta) end)
        {:noreply, channel}
      end

      def connect! do
        keys = routing_keys()

        if Enum.empty?(keys) do
          FluxAMQP.connect!(__MODULE__)
        else
          FluxAMQP.connect!(__MODULE__, routing_keys: keys)
        end
      end
    end
  end

  @callback consume(channel :: Channel.t(), payload :: String.t(), meta :: map) :: any
  @callback routing_keys :: list(String.t() | {String.t(), String.t()})
end
