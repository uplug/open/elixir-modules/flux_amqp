defmodule FluxAMQP.Error.Messages do
  @moduledoc false
  @moduledoc since: "0.0.5"

  @messages %{
    failed_to_close_connection: "failed to disconnect from AMQP broker",
    failed_to_connect: "failed to connect to AMQP broker",
    failed_to_open_channel: "failed to open AMQP channel",
    failed_to_send_amqp_message: "failed to send AMQP message"
  }

  @spec get(atom) :: String.t()
  def get(reason) do
    Map.get(@messages, reason, "")
  end
end
