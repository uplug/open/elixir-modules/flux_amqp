defmodule FluxAMQP.Config.Broker do
  @moduledoc false
  @moduledoc since: "0.0.1"

  alias FluxAMQP.Config.Broker
  alias FluxAMQP.Config.Broker.Connection

  @type t :: %Broker{
          uri: String.t(),
          connection: Connection.t()
        }

  defstruct uri: "amqp://guest:guest@rabbitmq", connection: %Connection{}

  @doc since: "0.0.1"
  @spec fetch(Broker.t(), keyword) :: Broker.t()
  def fetch(broker \\ %Broker{}, opts \\ []) do
    case opts do
      [] -> broker
      opts -> struct(broker, uri: uri(broker, opts), connection: connection(broker, opts))
    end
  end

  defp uri(broker, opts) do
    Keyword.get(opts, :uri, broker.uri)
  end

  defp connection(broker, opts) do
    Connection.fetch(broker.connection, Keyword.get(opts, :connection, []))
  end
end
