defmodule FluxAMQP.Config.Broker.Connection do
  @moduledoc false
  @moduledoc since: "0.0.1"

  alias FluxAMQP.Config.Broker.Connection
  alias FluxAMQP.Config.Broker.Connection.Reattempt

  @type t :: %Connection{
          prefetch_count: integer,
          reattempt_connection_on_failure?: boolean,
          reattempt: Reattempt.t()
        }

  defstruct prefetch_count: 50, reattempt_connection_on_failure?: true, reattempt: %Reattempt{}

  @doc since: "0.0.1"
  @spec fetch(Connection.t(), keyword) :: Connection.t()
  def fetch(connection \\ %Connection{}, opts \\ []) do
    case opts do
      [] ->
        connection

      opts ->
        struct(
          connection,
          prefetch_count: prefetch_count(connection, opts),
          reattempt_connection_on_failure?: reattempt_connection_on_failure(connection, opts),
          reattempt: reattempt(connection, opts)
        )
    end
  end

  defp prefetch_count(connection, opts) do
    Keyword.get(opts, :prefetch_count, connection.prefetch_count)
  end

  defp reattempt_connection_on_failure(connection, opts) do
    Keyword.get(
      opts,
      :reattempt_connection_on_failure?,
      connection.reattempt_connection_on_failure?
    )
  end

  defp reattempt(connection, opts) do
    Reattempt.fetch(connection.reattempt, Keyword.get(opts, :reattempt, []))
  end
end
