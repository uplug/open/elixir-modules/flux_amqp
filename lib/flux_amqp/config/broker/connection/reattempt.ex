defmodule FluxAMQP.Config.Broker.Connection.Reattempt do
  @moduledoc false
  @moduledoc since: "0.0.1"

  alias FluxAMQP.Config.Broker.Connection.Reattempt

  @type t :: %Reattempt{
          maximum_attempts: integer | :infinity,
          waiting_ms: integer
        }

  defstruct maximum_attempts: :infinity, waiting_ms: 10_000

  @doc since: "0.0.1"
  @spec fetch(Reattempt.t(), keyword) :: Reattempt.t()
  def fetch(reattempt \\ %Reattempt{}, opts \\ []) do
    struct(reattempt, opts)
  end
end
