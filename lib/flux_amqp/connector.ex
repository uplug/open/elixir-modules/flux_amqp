defmodule FluxAMQP.Connector do
  @moduledoc false
  @moduledoc since: "0.0.1"

  alias AMQP.{Basic, Channel, Connection, Queue}
  alias FluxAMQP.{Config, Error}

  require Logger

  @doc since: "0.0.1"
  @spec close_connection(Connection.t()) :: :ok | {:error, :failed_to_close_connection}
  def close_connection(connection) do
    close_connection!(connection)
  rescue
    error -> Error.to_tuple(error)
  end

  @doc since: "0.0.5"
  @spec close_connection!(Connection.t()) :: :ok
  def close_connection!(connection) do
    :ok = Connection.close(connection)
  rescue
    error -> reraise Error.from(error, :failed_to_close_connection), __STACKTRACE__
  end

  @doc since: "0.0.1"
  @spec connect(module, keyword) ::
          {:ok, Connection.t() | Channel.t()} | {:error, :failed_to_connect}
  def connect(consumer, opts \\ []) do
    {:ok, connect!(consumer, opts)}
  rescue
    error -> Error.to_tuple(error)
  end

  @doc since: "0.0.5"
  @spec connect!(module, keyword) :: Connection.t() | Channel.t()
  def connect!(consumer, opts \\ []) do
    do_connect!(consumer, fetch_opts(opts), opts)
  end

  @doc since: "0.0.5"
  @spec open_channel(Connection.t()) :: {:ok, Channel.t()} | {:error, :failed_to_open_channel}
  def open_channel(connection) do
    {:ok, open_channel!(connection)}
  rescue
    error -> Error.to_tuple(error)
  end

  @doc since: "0.0.5"
  @spec open_channel!(Connection.t()) :: Channel.t()
  def open_channel!(connection) do
    {:ok, channel} = Channel.open(connection)
    channel
  rescue
    error -> reraise Error.from(error, :failed_to_open_channel), __STACKTRACE__
  end

  defp fetch_opts(opts) do
    %Config{}
    |> Config.fetch(Application.get_all_env(:flux_amqp))
    |> Config.fetch(opts)
  end

  @opts_keys [:only_connect?, :attempts]

  defp do_connect!(consumer, config, opts) do
    config.broker.uri
    |> open_connection()
    |> maybe_configure_channel(consumer, config, Keyword.take(opts, @opts_keys))
  rescue
    error -> handle_connection_error!(error, consumer, config, opts, __STACKTRACE__)
  end

  defp open_connection(uri) do
    {:ok, connection} = Connection.open(uri)
    connection
  end

  defp maybe_configure_channel(connection, consumer, config, opts) do
    if Keyword.get(opts, :only_connect?) == true do
      connection
    else
      connection
      |> open_connection_channel()
      |> consume_queues(config, Process.whereis(consumer))
    end
  end

  defp open_connection_channel(connection) do
    {:ok, channel} = open_channel(connection)
    channel
  end

  defp consume_queues(channel, config, consumer_pid) do
    :ok = Basic.qos(channel, prefetch_count: config.broker.connection.prefetch_count)
    Enum.reduce(config.routing_keys, channel, &consume_queue(&2, &1, consumer_pid))
  end

  defp consume_queue(channel, route, consumer_pid) do
    {:ok, _queue} = Queue.declare(channel, route, durable: true)
    {:ok, _consumer_tag} = Basic.consume(channel, route, consumer_pid)
    channel
  end

  defp handle_connection_error!(error, consumer, config, opts, stacktrace) do
    Logger.error("AMQP '#{consumer}' connection failure",
      error_stacktrace: Exception.format(:error, error, stacktrace)
    )

    if config.broker.connection.reattempt_connection_on_failure? do
      retry_connection!(error, consumer, config, opts, stacktrace)
    else
      raise_connection_failure!(error, stacktrace)
    end
  end

  defp retry_connection!(error, consumer, config, opts, stacktrace) do
    %{maximum_attempts: max, waiting_ms: waiting_ms} = config.broker.connection.reattempt

    if max == :infinity do
      Logger.info("New AMQP '#{consumer}' connection attempt in #{waiting_ms}ms")
      Process.sleep(waiting_ms)
      do_connect!(consumer, config, opts)
    else
      att = Keyword.get(opts, :attempts, 0) + 1

      if att < max do
        Logger.info("AMQP '#{consumer}' connection attempt #{att + 1}/#{max} in #{waiting_ms}ms")
        Process.sleep(waiting_ms)
        do_connect!(consumer, config, Keyword.put(opts, :attempts, att))
      else
        raise_connection_failure!(error, stacktrace)
      end
    end
  end

  defp raise_connection_failure!(error, stacktrace) do
    reraise Error.from(error, :failed_to_connect), stacktrace
  end
end
