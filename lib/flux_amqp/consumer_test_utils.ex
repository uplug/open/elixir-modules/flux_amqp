defmodule FluxAMQP.ConsumerTestUtils do
  @moduledoc """
  Simplify the creation of test cases for modules using `FluxAMQP`
  """
  @moduledoc since: "0.0.4"

  alias FluxAMQP.{Connector, ConsumerTestUtils}

  import ExUnit.Assertions
  import ExUnit.CaptureLog

  @type t :: %ConsumerTestUtils{
          consumer: module | nil,
          log_messages: list(String.t()),
          meta: map,
          payload: String.t(),
          result: any
        }

  defstruct consumer: nil,
            log_messages: [],
            meta: %{},
            payload: "",
            do_before_function: nil,
            result: nil

  @doc """
  Add a log message which will be tested after the execution of the consumer.

  ## Parameters

  - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
    test.

  - `message` - A `t:String.t/0` log message.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils =
      ...>   MyConsumer
      ...>   |> setup_consumer_test()
      ...>   |> add_log_message("MyConsumer received a message")
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.4"
  @spec add_log_message(ConsumerTestUtils.t(), String.t()) :: Tunnel.t()
  def add_log_message(utils, message) do
    struct(utils, log_messages: [message] ++ utils.log_messages)
  end

  @doc """
  Execute a test case which will assert the result and the log messages of
  `c:FluxAMQP.Consumer.consume/3` implementation call made by the consumer
  module.

  ## Parameters

  - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
    test.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils =
      ...>   FluxAMQP.ConsumerTestUtils.MyConsumer
      ...>   |> setup_consumer_test()
      ...>   |> set_meta(%{routing_key: "test.route"})
      ...>   |> set_payload(~s({"success": true}))
      ...>   |> do_before(fn _utils, _channel -> :ok end)
      ...>   |> set_result(:ok)
      ...>   |> add_log_message("MyConsumer received a message")
      ...>   |> consumer_test()
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.4"
  @spec consumer_test(ConsumerTestUtils.t()) :: ConsumerTestUtils.t()
  def consumer_test(utils) do
    operation = fn ->
      {:ok, channel} = Connector.connect(__MODULE__)
      maybe_do_before(utils, channel)
      assert run_consumer(utils, channel) == utils.result
    end

    log = capture_log(operation)

    Enum.each(utils.log_messages, &assert(log =~ &1))

    utils
  end

  defp maybe_do_before(%{do_before_function: do_before_function} = utils, channel) do
    if do_before_function != nil do
      do_before_function.(utils, channel)
    end

    :ok
  end

  defp run_consumer(utils, channel) do
    apply(
      utils.consumer,
      :consume,
      [
        channel,
        utils.payload,
        utils.meta
      ]
    )
  end

  @doc """
  Set a `t:function/0` which will be executed before the consumption assertion.

  ## Parameters

  - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
    test.

  - `do_before_function` - A `t:function/0` with two parameters:

    - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
      test.

    - `channel` - Consumer `t:AMQP.Channel.t/0`.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils =
      ...>   MyConsumer
      ...>   |> setup_consumer_test()
      ...>   |> set_meta(%{routing_key: "test"})
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.5"
  @spec do_before(ConsumerTestUtils.t(), (ConsumerTestUtils.t(), AMQP.Channel.t() -> any)) ::
          ConsumerTestUtils.t()
  def do_before(utils, do_before_function) do
    struct(utils, do_before_function: do_before_function)
  end

  @doc """
  Set a `t:map/0` that behaves as the meta data when one AMQP message is
  received.

  ## Parameters

  - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
    test.

  - `meta` - A `t:map/0` that behaves as the AMQP message meta.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils =
      ...>   MyConsumer
      ...>   |> setup_consumer_test()
      ...>   |> set_meta(%{routing_key: "test"})
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.4"
  @spec set_meta(ConsumerTestUtils.t(), map) :: Test.Utils.t()
  def set_meta(utils, meta) do
    struct(utils, meta: meta)
  end

  @doc """
  Set a `t:String.t/0` that behaves as the AMQP message received by the
  consumer.

  ## Parameters

  - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
    test.

  - `payload` - A `t:String.t/0` that behaves as the AMQP message.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils =
      ...>   MyConsumer
      ...>   |> setup_consumer_test()
      ...>   |> set_payload(~s({"hello": "world"}))
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.4"
  @spec set_payload(ConsumerTestUtils.t(), String.t()) ::
          ConsumerTestUtils.t()
  def set_payload(utils, payload) do
    struct(utils, payload: payload)
  end

  @doc """
  Create `t:FluxAMQP.ConsumerTestUtils.t/0`.

  This is the starting function to a consumer pipe test case.

  ## Parameters

  - `consumer` - A module which uses `FluxAMQP.Consumer`.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils = setup_consumer_test(MyConsumer)
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.4"
  @spec set_result(ConsumerTestUtils.t(), any) :: ConsumerTestUtils.t()
  def set_result(utils, result) do
    struct(utils, result: result)
  end

  @doc """
  Set the result expected to receive after a `c:FluxAMQP.Consumer.consume/3`
  implementation call.

  ## Parameters

  - `utils` - `t:FluxAMQP.ConsumerTestUtils.t/0` with the data for a consumer
    test.

  - `result` - Expected result for the current consumer test.

  ## Examples

      iex> import FluxAMQP.ConsumerTestUtils
      ...> utils =
      ...>   MyConsumer
      ...>   |> setup_consumer_test()
      ...>   |> set_result(:ok)
      ...> with %FluxAMQP.ConsumerTestUtils{} <- utils, do: :passed
      :passed
  """
  @doc since: "0.0.4"
  @spec setup_consumer_test(module) :: ConsumerTestUtils.t()
  def setup_consumer_test(consumer) do
    %ConsumerTestUtils{consumer: consumer}
  end
end
