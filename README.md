# Flux AMQP

[![pipeline status](https://gitlab.com/uplug/open/elixir-modules/flux_amqp/badges/master/pipeline.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_amqp/commits/master)
[![coverage report](https://gitlab.com/uplug/open/elixir-modules/flux_amqp/badges/master/coverage.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_amqp/commits/master)

Interface to connect to AMQP broker, sending and retrieving messages.

It uses `AMQP`, check their documentation to understand
how this library perform the connection, configuration, consumption and delivery of
AMQP messages.

## Usage

Add [Flux AMQP](https://hex.pm/packages/flux_amqp) as a dependency in your
`mix.exs` file:

```elixir
def deps do
  [{:flux_amqp, "~> 0.0.5"}]
end
```

`FluxAMQP` describes how to define and close a connection and how to send a message.

`FluxAMQP.Consumer` describes how to consume AMQP messages.

## Application Configuration

```elixir
import Config

# Default values
config :flux_amqp,
  broker: [
    uri: "amqp://guest:guest@rabbitmq",
    connection: [
      prefetch_count: 50,
      reattempt_connection_on_failure?: true,
      reattempt: [
        maximum_attempts: :infinity,
        waiting_ms: 10_000
      ]
    ]
  ],
  routing_keys: []
```

### Configuration Options

- `:broker` - Set the AMQP broker. Accepts `t:keyword/0`. Options are:

  - `:uri` - The AMQP Broker URI. Defaults to `amqp://guest:guest@rabbitmq`.
    Accepts `t:String.t/0`.

  - `:connection` - Set the AMQP broker connection. Accepts `t:keyword/0`.
    Options are:

    - `:prefetch_count` - Set the maximum amount of non-acknowledged messages
      each consumer will retain. Defaults to `50`. Accepts `t:integer/0`.

    - `:reattempt_connection_on_failure?` - If `true`, when a connection to
      AMQP broker fails, a reattempt will be performed. Defaults to `true`.
      Accepts `t:boolean/0`.

    - `:reattempt` - Set the reattempt settings. Accepts `t:keyword/0`.
      Options are:

      - `:maximum_attempts` - How many attempts should be performed in case of
        connection failure. If set to `:infinity`, it will try until connection
        succeeds. Defaults to `:infinity`. Accepts `t:integer/0` or
        `:infinity`.

      - `:waiting_ms` - How many milliseconds should wait before reattempting
        connection. Defaults to `10_000`. Accepts `t:integer/0`.

- `:routing_keys` - The AMQP routing keys which the messages will be consumed.
  Defaults to an emptt list. Accepts `t:String.t/0`.
