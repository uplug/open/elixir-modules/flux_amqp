import Config

config :flux_amqp,
  broker: [
    uri: "amqp://guest:guest@rabbitmq",
    connection: [
      prefetch_count: 50,
      reattempt_connection_on_failure?: true,
      reattempt: [
        maximum_attempts: :infinity,
        waiting_ms: 10_000
      ]
    ]
  ],
  routing_keys: []
