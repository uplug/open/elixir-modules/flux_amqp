import Config

config :logger,
  backends: [:console],
  level: :debug,
  truncate: :infinity,
  discard_threshold: 5_000,
  handle_otp_reports: false

config :logger, :console,
  format: "[$level$levelpad] $message $metadata\n",
  metadata: [:error_stacktrace]

config :lager,
  error_logger_redirect: false
