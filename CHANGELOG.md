# Changelog

## 0.0.5 - 2019-11-25

- **Added**:

  - `FluxAMQP.Error`, the error struct for `!` functions.
  - The following `!` functions:

    - `FluxAMQP.close_connection!/1`

    - `FluxAMQP.connect!/2`

    - `FluxAMQP.send!/3`

    - `FluxAMQP.open_channel!/1`

  - `FluxAMQP.ConsumerTestUtils.do_before/2`, to allow connection manipulation
    before the assertions.

  - `:prefetch_count` connection application config.

- **Changed**:

  - `FluxAMQP` connections now acts as
    [work queues](https://www.rabbitmq.com/tutorials/tutorial-two-elixir.html).

- **Removed**:

  - Exchange options on connections.
  - Exchange settings on application configuration `:routing_keys`.

## 0.0.4 - 2019-11-07

- **Added**:

  - `FluxAMQP.ConsumerTestUtils`, a module created to simplify the creation of
    test cases for modules using `FluxAMQP.Consumer`.

- **Changed**:

  - `FluxAMQP.Consumer` Default exchange changed from `amq.topic` to
    `amq.direct`.

## 0.0.3 - 2019-09-03

- **Changed**:

  - Callback `c:FluxAMQP.Consumer.consume/3` return type changed from `t:atom/0`
    to `t:any/0`.

- **Fixed**:

  - Fix conditional not working as expected on `FluxAMQP.Consumer`.

## 0.0.2 - 2019-09-01

- **Changed**:

  - Every logger now uses metadata to describe information. There are three
    metadata keys that can be filtered on logger settings:

    1. `:flux_amqp_params` - The params received by function.
    2. `:flux_amqp_result` - The result of the function.
    3. `:error` - The error result of the function.

## 0.0.1 - 2019-08-31

- **Added**:

  - The project is available on GitLab and Hex.

- **Notes**:

  - Minor changes (0.0.x) from the current version will be logged to this file.

  - When a major change is released (0.x.0 or x.0.0), the changelog of the
    previous major change will be grouped as a single change.
