defmodule FluxAMQP.DocTest do
  use ExUnit.Case, async: true

  alias FluxAMQP.{ConsumerTestUtils, Error}

  @moduletag :capture_log

  doctest ConsumerTestUtils
  doctest Error
  doctest FluxAMQP
end
