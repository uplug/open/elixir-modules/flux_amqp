defmodule FluxAMQPTest do
  use ExUnit.Case, async: true

  import ExUnit.CaptureLog

  describe "FluxAMQP.close_connection/1" do
    test "succeeds on valid connection" do
      connection = FluxAMQP.connect!(__MODULE__, only_connect?: true)
      assert FluxAMQP.close_connection(connection) == :ok
    end

    test "fails if invalid connection" do
      assert FluxAMQP.close_connection(nil) == {:error, :failed_to_close_connection}
    end
  end

  describe "FluxAMQP.connect/2" do
    test "succeeds on valid connection" do
      opts = [
        broker: [
          uri: "amqp://guest:guest@rabbitmq"
        ],
        routing_keys: [
          "test.direct",
          "test.topic"
        ]
      ]

      assert {:ok, %AMQP.Channel{}} = FluxAMQP.connect(__MODULE__, opts)
    end

    test "succeeds on valid connection with only_connect? option as true" do
      assert {:ok, %AMQP.Connection{}} = FluxAMQP.connect(__MODULE__, only_connect?: true)
    end

    test "fails if invalid AMQP Broker URI and no reattempt set" do
      opts = [
        broker: [
          uri: "invalid_broker_uri",
          connection: [
            reattempt_connection_on_failure?: false
          ]
        ]
      ]

      operation = fn ->
        assert FluxAMQP.connect(__MODULE__, opts) == {:error, :failed_to_connect}
      end

      assert capture_log(operation) =~ "connection failure"
    end

    test "will try indefinitely to connect if something went wrong" do
      opts = [
        broker: [
          uri: "invalid_broker_uri",
          connection: [
            reattempt: [
              waiting_ms: 250
            ]
          ]
        ]
      ]

      operation = fn ->
        task = Task.async(fn -> FluxAMQP.connect(__MODULE__, opts) end)
        assert Task.yield(task, 400) == nil
        Task.shutdown(task, :brutal_kill)
      end

      assert capture_log(operation) =~ "connection failure"
    end

    test "will try to connect accordingly to maximum_attempts if something went wrong" do
      opts = [
        broker: [
          uri: "invalid_broker_uri",
          connection: [
            reattempt: [
              waiting_ms: 250,
              maximum_attempts: 3
            ]
          ]
        ]
      ]

      operation = fn ->
        assert FluxAMQP.connect(__MODULE__, opts) == {:error, :failed_to_connect}
      end

      assert capture_log(operation) =~ "connection attempt 3/3 in 250ms"
    end
  end

  describe "FluxAMQP.send/3" do
    test "succeeds on valid connection" do
      payload = "payload"
      routing_key = "test.route"
      assert FluxAMQP.send(payload, routing_key) == :ok
    end

    test "succeeds on valid manual connection" do
      payload = "payload"
      routing_key = "test.route"
      connection = FluxAMQP.connect!(__MODULE__, only_connect?: true)
      channel = FluxAMQP.open_channel!(connection)
      opts = [channel: channel, connection: connection, close_connection?: true]
      assert FluxAMQP.send(payload, routing_key, opts) == :ok
    end

    test "returns {:error, reason} if failed to send message" do
      opts = [
        broker: [
          uri: "invalid_broker_uri",
          connection: [
            reattempt_connection_on_failure?: false
          ]
        ]
      ]

      payload = "payload"
      routing_key = "test.route"

      operation = fn ->
        assert FluxAMQP.send(payload, routing_key, opts) == {:error, :failed_to_send_amqp_message}
      end

      assert capture_log(operation) =~ "connection failure"
    end
  end

  describe "FluxAMQP.open_channel/1" do
    test "fails if invalid connection" do
      assert FluxAMQP.open_channel(nil) == {:error, :failed_to_open_channel}
    end
  end
end
